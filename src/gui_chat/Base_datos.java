/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui_chat;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 * Clase Base_datos
 * @author Raúl, Nico, Iván, Romen
 */
public class Base_datos {
    Connection connection = null;
    
    /**
     * Método que se encarga de establecer la conexión con la base de datos, cargando la configuración desde un archivo properties
     * @param error Devuelve error en caso de fallo del método
     * @return Devuelve true si conexión es correcta, false si no
     */
    
    public boolean Conectar (String [] error) {
        boolean ret = true;
        try{
            Properties config = new Properties();
            Class clase = this.getClass();
            InputStream inputstream
                    = clase.getResourceAsStream("/recursos/0.0.properties");
            config.load(inputstream);
            String jdbc_url = config.getProperty("jdbc_url");
            String jdbc_usuario = config.getProperty("jdbc_usuario");
            String jdbc_clave = config.getProperty("jdbc_clave");
            String jdbc_clave_descodificada = decodificar(jdbc_clave);
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connection = DriverManager.getConnection(
                    jdbc_url,
                    jdbc_usuario,
                    jdbc_clave_descodificada);
        }catch(Exception e){
                String mensaje = e.getMessage();
                if (mensaje == null){
                    mensaje = "";
                }
                error [0] = "Error al conectar con la base de datos."+mensaje;
                ret = false;
        }
        return ret;
    }
    
    /**
     * Método que se encarga de establecer la desconexión con la base de datos
     * @param error Devuelve error en caso de fallo del método
     * @return Devuelve true si desconexión es correcta, false si no
     */
    public boolean Desconectar (String [] error){
        boolean ret = true;
        try{
            connection.close();
            connection = null;
        }catch(Exception e){
            String mensaje = e.getMessage();
            if(mensaje == null){
                mensaje = "";
            }
            error [0] = "Error al desconectar con la base de datos. "+mensaje;
            ret = false;
        }
        return ret;
    }
    
    /**
     * Método que se encarga de decodificar la contraseña del properties
     * @param jdbc_clave Clave que se carga desde el properties y será decodificada
     * @return Retorna la clave decodificada 
     */
    public String decodificar (String jdbc_clave)
    {
        String jdbc_clave_decodificada = "";
        int longitud_cadena = jdbc_clave.length();
        int i = 0;
        for(i=0; i<longitud_cadena; i++){
            if (jdbc_clave.substring(i,i+1).equals("1")){
                jdbc_clave_decodificada=jdbc_clave_decodificada+"i";
            } else if (jdbc_clave.substring(i,i+1).equals("2")){
                jdbc_clave_decodificada=jdbc_clave_decodificada+"u";
            } else if (jdbc_clave.substring(i,i+1).equals("3")){
                jdbc_clave_decodificada=jdbc_clave_decodificada+"e";
            } else if (jdbc_clave.substring(i,i+1).equals("4")){
                jdbc_clave_decodificada=jdbc_clave_decodificada+"a";
            } else if (jdbc_clave.substring(i,i+1).equals("5")){
                jdbc_clave_decodificada=jdbc_clave_decodificada+"A";
            } else if (jdbc_clave.substring(i,i+1).equals("6")){
                jdbc_clave_decodificada=jdbc_clave_decodificada+"E";
            } else if (jdbc_clave.substring(i,i+1).equals("7")){
                jdbc_clave_decodificada=jdbc_clave_decodificada+"I";
            } else if (jdbc_clave.substring(i,i+1).equals("8")){
                jdbc_clave_decodificada=jdbc_clave_decodificada+"O";
            } else if (jdbc_clave.substring(i,i+1).equals("9")){
                jdbc_clave_decodificada=jdbc_clave_decodificada+"U";
            } else if (jdbc_clave.substring(i,i+1).equals("0")){
                jdbc_clave_decodificada=jdbc_clave_decodificada+"o";
            } else {
                jdbc_clave_decodificada=jdbc_clave_decodificada+jdbc_clave.substring(i,i+1);
            }
        }
        return jdbc_clave_decodificada;
    }
}
