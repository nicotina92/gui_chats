/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui_chat;

import com.sun.javafx.font.FontConstants;
import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Random;

/**
 * Clase Con_chats
 * @author Raúl, Nico, Iván, Romen
 */
public class Con_chats {
    
    /**
     * Método que recibe el usuario y texto para realizar el envío de la conversación a la base de datos: chats
     * @param connection Conexión con la base de datos que recibimos
     * @param texto String que contiene el texto a enviar a la base de datos
     * @param lista_usuarios List que contiene el usuario que ha iniciado sesión para registrar su mensaje
     * @param error Devuelve error en caso de fallo del método
     * @return Devuelve true si todo correcto, false si no
     */
    
    public static boolean enviar_mensaje(Connection connection, String texto, List lista_usuarios, String [] error) {
        boolean ret = true;
        String comando = "insert into conversaciones (texto, ref_usuario, ref_chat) values (?,?,?)";
        try {
            int id_chat = 1;
            PreparedStatement preparedstatement = connection.prepareStatement(comando);
            preparedstatement.setString(1, texto);
            preparedstatement.setString(2,(String) lista_usuarios.get(0));
            preparedstatement.setInt(3, id_chat);
            preparedstatement.executeUpdate();
            preparedstatement.close();
        }
        catch (Exception e){
            String mensaje = e.getMessage();
            if (mensaje==null){
                mensaje = "Error";
            }
            error[0]= "Error al enviar el mensaje" + mensaje;
            ret=false;
        }
        return ret;
    }
    
    /**
     * Método que lee los mensajes de la base de datos, para capturar la referencia del usuario y texto de la tabla conversaciones
     * @param connection Conexión con la base de datos que recibimos
     * @param error Devuelve error en caso de fallo del método
     * @return Devuelve true si todo correcto, false si no
     */
    public static boolean leer_mensajes (Connection connection,int ultimo_id_conversacion,List <Map <String, String>> lista_resultados, String [] error){
        boolean ret = true;
        String comando ="select id_conversacion, texto, ref_usuario from conversaciones where id_conversacion > ? and ref_chat=? order by id_conversacion asc";
        

        if( ultimo_id_conversacion ==0){
            comando = comando +" "+"limit 50";
        }
        try{
            PreparedStatement preparedstatement = connection.prepareStatement(comando);
            preparedstatement.setInt(1, ultimo_id_conversacion);
            preparedstatement.setInt(2, 1);
            ResultSet resultado = preparedstatement.executeQuery();
            while (resultado.next()){
                Map <String,String> mapa_resultado = new HashMap();
                /*mapa_resultado.put("id_conversacion", resultado.getString(1));*/
                Gui_chats.ultimo_id_conversacion=resultado.getInt(1);
                mapa_resultado.put("texto", resultado.getString(2));
                mapa_resultado.put("ref usuario", resultado.getString(3));
                lista_resultados.add(mapa_resultado);
            }
            
        }
        catch (Exception e){
            String mensaje = e.getMessage();
            if (mensaje==null){
                mensaje = "Error";
            }
            error[0]= "Error al leer el mensaje" + mensaje;
            ret=false;
        }
        return ret;
    }
    
    public static String decorar_html (List <Map <String, String>> lista_resultados, String codigo_html,List lista_usuarios ){
        int i=0;
        String html = "<span color=";
        List <String> colores = new ArrayList();
        colores.add("#FD0707");
        colores.add("#0066CC");
        colores.add("#66FF66");
        List <Map <String,String>> usuarios_color = new ArrayList();
        Map <String,String> mapa_usuario_color=new HashMap();
        for (Map <String,String> mapa_resultado : lista_resultados){
            String usuario = mapa_resultado.get("ref usuario");
            for (Map <String,String> mapa_colores : usuarios_color){
                if (mapa_colores.containsValue(usuario)){
                    continue;
                }
                else{
                    mapa_usuario_color.put("usuario", usuario);
                    if (colores.isEmpty()){
                        continue;
                    }
                    else{
                        mapa_usuario_color.put("color", colores.get(i));
                        colores.remove(i);
                    }
                }
                usuarios_color.add(mapa_usuario_color);
            }
            i++;
            
            
        }
        String color="";
        for (Map <String,String> mapa_resultado : lista_resultados){
            String usuario=mapa_resultado.get("ref usuario");
            for (Map <String,String> mapa_color : usuarios_color){
                if (mapa_color.containsValue(usuario)){
                    color = mapa_color.get("color");
                }
            }
            
            String texto_user = "<span color="+color+">"+usuario+"</span>";
            codigo_html=codigo_html+texto_user+"\n"+mapa_resultado.get("texto")+"\n\n";
        }
        
        return codigo_html;
    }
}
