/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui_chat;

import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Clase Con_entradas
 * @author Raúl, Nico, Iván, Romen
 */
public class Con_entradas {
    
    /**
     * Método que comprueba que el usuario-contraseña existen en la base de datos
     * @param connection Conexión con la base de datos que recibimos
     * @param usuario Recibe el usuario introducido para comprobar si existe en la base de datos: chats
     * @param clave Recibe la clave introducida para comprobar si existe en la base de datos: chats
     * @param error Devuelve error en caso de fallo del método
     * @return Devuelve true si todo correcto, false si no
     */
    
    public static boolean comprobar_usuario (Connection connection, String usuario, String clave, String [] error){
        boolean ret=true;
        String comando = "select id_usuario_email from usuarios where id_usuario_email=? and clave=?";
        try{
            PreparedStatement preparedstatement = connection.prepareStatement(comando);
            preparedstatement.setString(1, usuario);
            preparedstatement.setString(2, clave);
            ResultSet resultado = preparedstatement.executeQuery();
            if (resultado.next()){
                ret=true;
            }
            else{
                ret=false;
            }
        }
        catch (Exception e){
            String mensaje=e.getMessage();
            if (mensaje==null){
                mensaje = "Error en la función de comprobar usuario";
            }
            error[0]=error[0]+mensaje;
            ret=false;
        }
        return ret;
    }
    
    /**
     * Método para comprobar si el usuario introducido tiene formato email
     * @param usuario Recibe el usuario introducido para comprobar si tiene formato email
     * @return Devuelve true si formato correcto, false si formato incorrecto
     */
    public static boolean validar_usuario (String usuario){
        Pattern pattern;
        Matcher matcher;
        String email_pattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(email_pattern);
        matcher = pattern.matcher(usuario);
        return matcher.matches();
    }
    
    /**
     * Método para codificar la clave introducida por el usuario mediante SHA-256
     * @param clave Recibe la clave introducida para codificarla
     * @param error Devuelve error en caso de fallo del método
     * @return Devuelve true si todo correcto, false si no 
     */
    public static String codificar_clave (String clave, String [] error){
        String resultado = "";
        try { 
            MessageDigest mDigest = MessageDigest.getInstance("SHA-256");
            byte[] digest = mDigest.digest(clave.getBytes());
            StringBuilder stringbuilder = new StringBuilder();
            String texto_hexadecimal;
            for (int i = 0; i < digest.length; i++) {
                texto_hexadecimal = Integer.toString((digest[i] & 0xff) + 0x100, 16 );
                texto_hexadecimal = texto_hexadecimal.substring(1);
                stringbuilder.append(texto_hexadecimal);
                resultado=resultado+texto_hexadecimal;
            }
        } catch (Exception e) {
            String mensaje = e.getMessage ();
            if (mensaje == null) {
                mensaje = "";
            }
            error [0] = "Error al calcular el código. " + mensaje;
        }
        return resultado;
    }
}
