/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui_chat;

import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author daw
 */
public class Temporizadores {
    /**
     * Prepara un evento que se ejecuta cada cierto tiempo 
     * @param intervalo milisegundos de tiempo entre eventos temporizados
     * @param error Mensaje de error
     * La palabra clave "synchronized" sirve para garantizar que
     * tareas simultaneas, no hacen uso del objeto, simultaneamente
     * (hace de semaforo)
     * @return 
     */
    
    public synchronized boolean lanzar_temporizador(long intervalo, String [] error){
        boolean ret=true;
        // Timer es la clase de los temporizadores
        Timer timer = new Timer();
        // TimerTask es la clase que maneja el temporizador: Timer
        // Construir una clase "anonima"
        // 1- Se usa new <constructor>
        // 2- Se pone entre llaves, detras, los metodos que se sobrecargan
        //    que se sustituyen (tienen la etiqueta @override)
        //3- Dentro de esos metodos se tiene acceso a los datos globales
        //   y a los de la clase en la que se declara
        TimerTask timertask = new TimerTask (){
            @Override
            public void run(){
                boolean ret=true;
                String [] error = {""};
                // llama a un metodo de la clase dentro de la que se creó
                ret = actualizar(error);
            }
        
        };
        //Programar el temporizador: <tarea>, <tiempo para el inicio>,<tiempo entre repeticiones>
        timer.schedule(timertask, 0, intervalo);
        
        return ret;
    }
    /**
     * Metodo que es llamado cuando se produce el evento temporizado
     * @param error Mensaje de error, si hay: sin cambios, si no hay
     * @return true si todo va bien, false si hay errores
     * Es "synchronized" porque podria ser llamado por varios
     * temporizadores concurrentemente
     */
    public synchronized boolean actualizar (String [] error){
        boolean ret =true;
        return ret;
    }
}
